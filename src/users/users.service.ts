import { Injectable } from '@nestjs/common';
import * as csvdb from 'csv-database';
import { generate } from 'randomstring';
@Injectable()
export class UserService {
  async create(data: any): Promise<any> {
    const db = await csvdb(
      'data.csv',
      [
        '_id',
        'name',
        'gender',
        'phone',
        'email',
        'address',
        'nationality',
        'date_of_birth',
      ],
      ',',
    );
    data._id = generate(12);
    return await db.add(data);
  }

  async update(_id: string, data: any): Promise<any> {
    const db = await csvdb(
      'data.csv',
      [
        '_id',
        'name',
        'gender',
        'phone',
        'email',
        'address',
        'nationality',
        'date_of_birth',
      ],
      ',',
    );
    return await db.edit({ _id }, data);
  }

  async getAll(): Promise<any> {
    const db = await csvdb(
      'data.csv',
      [
        '_id',
        'name',
        'gender',
        'phone',
        'email',
        'address',
        'nationality',
        'date_of_birth',
      ],
      ',',
    );
    return await db.get();
  }

  async getOne(_id: string): Promise<any> {
    const db = await csvdb(
      'data.csv',
      [
        '_id',
        'name',
        'gender',
        'phone',
        'email',
        'address',
        'nationality',
        'date_of_birth',
      ],
      ',',
    );
    return await db.get({ _id });
  }

  async find(object: Object): Promise<any> {
    const db = await csvdb(
      'data.csv',
      [
        '_id',
        'name',
        'gender',
        'phone',
        'email',
        'address',
        'nationality',
        'date_of_birth',
      ],
      ',',
    );
    return await db.get(object);
  }

  async delete(_id: string): Promise<any> {
    const db = await csvdb(
      'data.csv',
      [
        '_id',
        'name',
        'gender',
        'phone',
        'email',
        'address',
        'nationality',
        'date_of_birth',
      ],
      ',',
    );
    return await db.delete({ _id });
  }
}
