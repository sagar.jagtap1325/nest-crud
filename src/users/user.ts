import { IsEmail, IsNotEmpty, IsNumber, IsDateString } from 'class-validator';

export class CreateUser {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsDateString()
  date_of_birth: string;

  @IsNotEmpty()
  phone: string;
}

export class UpdateUser {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsDateString()
  date_of_birth: string;

  @IsNotEmpty()
  phone: string;
}
