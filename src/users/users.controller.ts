import {
  Controller,
  Get,
  Post,
  Delete,
  Patch,
  Param,
  Body,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import { UserService } from './users.service';
import { CreateUser, UpdateUser } from './user';
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/')
  findAll(): any {
    return this.userService.getAll();
  }

  @Get('/:id')
  findOne(@Param('id') id): any {
    return this.userService.getOne(id);
  }

  @Post('/')
  async create(@Body() userData: CreateUser): Promise<any> {
    const check = await this.userService.find({ email: userData.email });
    if (check.length) {
      throw new HttpException('Email already exists!', HttpStatus.CONFLICT);
    }
    return this.userService.create(userData);
  }

  @Patch(':id/update')
  async update(@Param('id') id, @Body() userData: UpdateUser): Promise<any> {
    const check = await this.userService.find({ _id: id });
    if (!check.length) {
      throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
    }
    return this.userService.update(id, userData);
  }

  @Delete(':id/delete')
  async deleteUser(@Param('id') id): Promise<any> {
    const check = await this.userService.find({ _id: id });
    if (!check.length) {
      throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
    }
    return this.userService.delete(id);
  }
}
